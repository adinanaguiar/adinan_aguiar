FROM openjdk:12
ADD target/docker-spring-boot-adinan_aguiar.jar docker-spring-boot-adinan_aguiar.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-adinan_aguiar.jar"]
